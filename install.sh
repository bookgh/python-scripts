#!/usr/bin/env sh

# python-scripts/install.sh


usage() { echo 'Usage: ./install.sh <script>'; exit 0; }

[ $# -eq 0 ] && usage

files=$@
[ $1 = 'all' ] && files=$(cd scripts; ls *)

for script in $files
do
    dest=$HOME/.local/bin/${script%.*}
    cp -vi scripts/${script%.*}.py $dest
    chmod 744 $dest
done
