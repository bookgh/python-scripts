# Python Scripts

Personal collection of scripts written in Python.

| Script               | Description                                   |
|----------------------|-----------------------------------------------|
| download-as-pdf      | download web page as PDF                      |
| image-download       | downloag images from webpage                  |
| lc-progressbar       | progress bar for tasks adding lines to a file |
| plot                 | plot data from a column file                  |
| tplot                | plot data in the terminal from a column file  |
| pulse-volume-watcher | monitor audio volume with PulseAudio          |

## Installation

```bash
git clone https://gitlab.com/loicreynier/python-scripts.git
cd python-scripts
# ./install.sh <script> or ./install.sh all
./install.sh image-download
```
