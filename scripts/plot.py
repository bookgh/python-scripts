#!/usr/bin/env python3


"""Plot data from a column file.

This script uses Matplotlib to plot data from a column file. Columns to
plot are specified as tuple and are all plotted on the same axis. As an
example to plot column 1 (y axis) versus column 0 (x axis) one needs to
use `plot <file> 0,1`. The figure can be exported using the
`--save <savefile>` argument.

Example
-------
The following plots column 4 versus column 0 and column 3 versus column
0 from `data.xy` and saves the figure as `test.pdf`.

    $ python3 plot.py --save test.pdf data.xy 0,4 0,3

"""

from sys     import argv
from ast     import literal_eval as make_tuple
from os.path import basename, splitext
import argparse
import numpy as np
import matplotlib.pyplot as plt

def arg_parser():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        prog=splitext(basename(argv[0]))[0],
        description='Plot data from a colum file')
    parser.add_argument('file', type=str, help='column file')
    parser.add_argument('columns_to_plot', type=str, nargs='*',
                        help='colums to plot, e.g. `1,2`')
    parser.add_argument('--save', type=str, metavar='savefile',
                        help='export the figure to this file')
    parser.add_argument('--grid', action='store_true',
                        help='add a grid to the axis')
    return parser.parse_args()

def main(filepath, columns_to_plot, save=None, grid=False):
    """Open and plot file."""
    with open(filepath) as file_:
        array = np.loadtxt(file_)
    fig, axe = plt.subplots()
    for cols in columns_to_plot:
        cols = make_tuple(cols)
        axe.plot(array[:,cols[0]], array[:,cols[1]])
    if grid:
        axe.grid()
    if save is not None:
        fig.savefig(save)
        print(f'Figure exported to {save}')
    else:
        plt.show()

if __name__ == '__main__':
    args = arg_parser()
    main(args.file, args.columns_to_plot, save=args.save, grid=args.grid)
