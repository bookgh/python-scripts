#!/usr/bin/env python3


"""Bookmark manager."""

__version__ = '1'

import argparse
import sys
import csv
from os import system, linesep
from os.path import basename, splitext, expanduser

prog = splitext(basename(sys.argv[0]))[0]
store  = expanduser('~/.local/share/bookmarks/store.csv')
web_db = expanduser('~/.local/share/bookmarks/web.csv')
git_db = expanduser('~/.local/share/bookmarks/git.csv')
fieldnames = ['name', 'url', 'type', 'tag']

def add(args):
    """Add an entry to the store."""
    with open(store, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile, lineterminator=linesep)
        writer.writerow([args.name,args.url,args.type,args.tag])
    sort_store()
    update(args.type)

def sort_store():
    """Sort the store using `sort` command."""
    system(f'sort -u -t, -k3 -k4 -k1 -o {store} {store}')

def update_web():
    """Update the web bookmarks database from the store."""
    with (open(store, 'r', newline='\n') as csvfile,
          open(web_db, 'w', newline='') as webcsv):
        reader = csv.DictReader(csvfile, fieldnames=fieldnames)
        writer = csv.DictWriter(webcsv,
                                fieldnames=fieldnames[:2],
                                lineterminator='\n')
        for row in reader:
            if row['type'] == 'web':
                writer.writerow({'name': row['name'], 'url': row['url']})

def update_git():
    """Update the git bookmarks database from the store."""
    with (open(store, 'r', newline='\n') as csvfile,
          open(git_db, 'w', newline='') as gitcsv):
        reader = csv.DictReader(csvfile, fieldnames=fieldnames)
        writer = csv.DictWriter(gitcsv,
                                fieldnames=fieldnames[:2],
                                lineterminator='\n')
        for row in reader:
            if row['type'] == 'git':
                writer.writerow({'name': row['name'], 'url': row['url']})

def update(args):
    """Update a specific database from the store."""
    db = args if isinstance(args, str) else args.database
    if isinstance(db, list):
        db = db[0]
    if db == 'web':
        update_web()
    elif db == 'git':
        update_git()
    elif db == 'all':
        update_web()
        update_git()

def print_(args):
    """Print out a database."""
    db = args if isinstance(args, str) else args.database
    if isinstance(db, list):
        db = db[0]
    if db == 'all':
        cat(store)
    elif db == 'web':
        cat(web_db)
    elif db == 'git':
        cat(git_db)

def cat(file_):
    """Print out a text file."""
    with open(file_) as f:
        print(f.read(), end='')

def main():
    """Argument parser."""
    parser = argparse.ArgumentParser(prog=prog)
    parser.set_defaults(func=lambda x: parser.print_help())
    parser.add_argument(
            '--version',
            '-V',
            action='store_true',
            help='print version and exit')

    subparsers = parser.add_subparsers(
            title='Here are all the available subcommands',
            metavar='')
    parser_add = subparsers.add_parser(
            'add',
            aliases=['a'],
            help='add an entry to the bookmark store')
    parser_add.add_argument(
            'name',
            help='name in the bookmark store',
            type=str)
    parser_add.add_argument(
            'url',
            help='url',
            type=str)
    parser_add.add_argument(
            'type',
            metavar='type',
            help='git, web, youtube, rss',
            choices=['git', 'web', 'youtube', 'rss'])
    parser_add.add_argument('tag',
            help='tag',
            type=str)
    parser_add.set_defaults(func=add)

    parser_update = subparsers.add_parser(
            'update',
            aliases=['u'],
            help='update a specific database')
    parser_update.set_defaults(func=update)
    parser_update.add_argument(
            'database',
            help='database to update',
            nargs='*',
            choices=['all', 'git', 'web'],
            default='all')

    parser_print = subparsers.add_parser(
            'print',
            aliases=['p'],
            help='print a specific database')
    parser_print.set_defaults(func=print_)
    parser_print.add_argument(
            'database',
            help='database to print',
            nargs='*',
            choices=['all', 'git', 'web'],
            default='all')

    args = parser.parse_args()

    if args.version:
        print(f'{prog} ' +  __version__)
        sys.exit(0)

    args.func(args)

if __name__ == '__main__':
    main()
