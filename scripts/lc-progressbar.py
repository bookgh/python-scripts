#!/usr/bin/env python3


"""Line-count progressbar.

Progress bar for task whose progress can be measured by the number of
lines in the output file.

Example
-------

    $ lc-progressbar ~/simulation/output.dat 2500

"""

from time    import sleep
from sys     import argv
from os.path import basename, splitext

def count(file_):
    """Count number of lines in `file_`."""
    with open(file_) as f:
        return sum(1 for _ in f)

def progressbar(percent, length=60, fill='#', blank=' ', end=None):
    """Display the progress bar."""
    print_end = '\n' if percent == 100 else ''
    filled_length = int(length * percent // 100)
    progress = fill * filled_length + blank * (length - filled_length)
    print(f'\r[{progress}] {percent:.0f}% {end}', flush=True, end=print_end)

def main(file_, nmax, sleep_time=60):
    """Main loop tracking the file."""
    n = 0
    while n < nmax:
        n = count(file_)
        percent = int(n/nmax * 100)
        progressbar(percent, end=f'{n}/{nmax}')
        sleep(sleep_time)

if __name__ == '__main__':
    argv[0] = splitext(basename(argv[0]))[0]
    if len(argv[1:]) == 2:
        main(argv[1], int(argv[2]))
    elif len(argv[1:]) == 3:
        main(argv[1], int(argv[2]), int(argv[3]))
    else:
        print('Nothing to do')
        print(f'Usage: {argv[0]} <file> <nmax> [sleep_time]')
