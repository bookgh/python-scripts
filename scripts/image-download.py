#!/usr/bin/env python3


"""Script to download images from a webpage.

Example
-------

    $ python3 image-dl.py https://www.python.org

"""

import sys
import re
from os.path import basename, splitext
from urllib.request import urlretrieve
import requests
from bs4 import BeautifulSoup

def download_from(site, formats=['jpg', 'gif', 'png']):
    """
    Download all images from `site`.

    Parameters
    ----------
    site : string
        URL of the webpage.
    formats : list
        List of the image formats (jpg, png, gif, ...) to look for.
    """
    try:
        response = requests.get(site)
        response.raise_for_status()
    except requests.exceptions.HTTPError as err:
        print(err)

    formats = '|'.join(formats)

    soup = BeautifulSoup(response.text, 'html.parser')
    img_tags = soup.find_all('img')

    def helper(img):
        try:
            return img['src']
        except KeyError:
            pass

    urls = [helper(img) for img in img_tags if helper(img) is not None]

    for url in urls:
        filename = re.search(rf'/([\w_-]+[.]({formats}))$', url)
        if filename is None:
            print(f'Regex did not match with {url}')
            continue
        if url[0] == '/':
            base = re.search(r'.*\.[a-z]*/', site).group(0)
            if base[-1] == '/':
                base = base[:-1]
            url = f'{base}{url}'
        if 'http' not in url:
            url = f'{site}{url}'
        print(url)
        urlretrieve(url, filename.group(1))

if __name__ == '__main__':
    sys.argv[0] = splitext(basename(sys.argv[0]))[0]
    if len(sys.argv[1:]) != 0:
        for website in sys.argv[1:]:
            download_from(website)
    else:
        print('Nothing to do')
        print(f'Usage: {sys.argv[0]} <urls>')
