#!/usr/bin/env python3


"""Plot data in the terminal from a column file.

This script uses Termplotlib to plot data from a column file. Columns to
plot are specified as tuple and are all plotted on the same axis. As an
example to plot column 1 (y axis) versus column 0 (x axis) one needs to
use `tplot <file> 0,1`.

Example
-------
The following plots column 4 versus column 0 and column 3 versus column
0 from `data.xy`.

    $ python3 tplot.py data.xy 0,4 0,3

"""

from sys     import argv
from ast     import literal_eval as make_tuple
from os.path import basename, splitext
import argparse
import numpy as np
import termplotlib as tpl

def arg_parser():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        prog=splitext(basename(argv[0]))[0],
        description='Plot data from a colum file')
    parser.add_argument('file', type=str, help='column file')
    parser.add_argument('columns_to_plot', type=str, nargs='*',
                        help='colums to plot, e.g. `1,2`')
    return parser.parse_args()

def main(filepath, columns_to_plot):
    """Open and plot file."""
    with open(filepath) as file_:
        array = np.loadtxt(file_)
    fig = tpl.figure()
    for cols in columns_to_plot:
        cols = make_tuple(cols)
        fig.plot(array[:,cols[0]], array[:,cols[1]])
    fig.show()

if __name__ == '__main__':
    args = arg_parser()
    main(args.file, args.columns_to_plot)
