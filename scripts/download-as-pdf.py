#!/usr/bin/env python3


"""Download web page as PDF using pyppdf.

Example
-------

    $ python3 download-as-pdf.py https://www.python.org

"""

import sys
import re
from os.path import basename, splitext
import pyppdf

def download_from(url, filename=None):
    """Download `url` and save as `filename`.pdf."""
    if filename is None:
        filename = re.sub(r'^\w+://', '', url.lower()).replace('/', '-')
    if not filename.endswith('.pdf'):
        filename += '.pdf'

    pyppdf.save_pdf(filename, url)
    print(f'{url} -> {filename}')

if __name__ == '__main__':
    sys.argv[0] = splitext(basename(sys.argv[0]))[0]
    if len(sys.argv[1:]) != 0:
        if len(sys.argv[1:]) > 1:
            download_from(sys.argv[1], filename=sys.argv[2])
        else:
            download_from(sys.argv[1])
    else:
        print('Nothing to do')
        print(f'Usage: {sys.argv[0]} <url> <filename>')
